package com.aim.questionnaire.service;

import com.aim.questionnaire.common.Role;
import com.aim.questionnaire.common.utils.UUIDUtil;
import com.aim.questionnaire.dao.*;
import com.aim.questionnaire.dao.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class LoginAuthService {

    @Autowired
    private TotalRoleMapper totalRoleMapper;

    @Autowired
    private AdminMapper adminMapper;

    @Autowired
    private TenantMapper tenantMapper;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private AnswerMapper answerMapper;

    public Map<String, Object> pwLogin(String account, String password) {
        TotalRole query = TotalRole
                .QueryBuild()
                .fetchAll()
                .account(account)
                .build();
        TotalRole res = totalRoleMapper.queryTotalRoleLimit1(query);
        Map<String, Object> map = new HashMap<String, Object>();
        if (res == null) {
            return null;
        }
        if (!res.getPassword().equals(password)) {
            return null;
        }
        map.put("id", res.getId());
        map.put("role", Role.values()[res.getRole()].name().toLowerCase());
        map.put("token", UUIDUtil.getOneUUID());
        return map;
    }

    public Boolean sendSms(String phoneNumber, String purpose) {
        return true;
    }

    public Map<String, Object> smsLogin(String veriCode, String phoneNumber) {
        Map<String, Object> map = new HashMap<>();
        return map;
    }

    public Boolean register(Map<String, Object> map) {
        String account = (String) map.get("account");
        String password = (String) map.get("password");
        String phoneNumber = (String) map.get("phoneNumber");
        String veriCode = (String) map.get("veriCode");
        String role = (String) map.get("role");

        String securityQuestion = (String) map.get("securityQuestion");
        String securityAnswer = new String();
        if (securityQuestion != null) {
            securityAnswer = (String) map.get("securityAnswer");
        }
        String id = UUIDUtil.getOneUUID();
        TotalRole totalRole = TotalRole.Build()
                .id(id)
                .account(account)
                .password(password)
                .phone_number(phoneNumber)
                .role(Role.valueOf(role.toUpperCase()).ordinal())
                .security_question(securityQuestion)
                .security_answer(securityAnswer)
                .deleted(0)
                .build();
        totalRoleMapper.insertTotalRole(totalRole);
        switch (role) {
            case "admin":
                Admin admin = Admin.Build()
                        .id(id)
                        .account(account)
                        .password(password)
                        .build();
                adminMapper.insertAdmin(admin);
                break;
            case "tenant":
                Tenant tenant = Tenant.Build()
                        .id(id)
                        .bill(0.0)
                        .build();
                tenantMapper.insertTenant(tenant);
                break;
            case "user":
                String tenantAccount = (String) map.get("tenantAccount");
                User user = User.Build()
                        .id(id)
                        .tenant_id(tenantAccount)
                        .build();
                userMapper.insertUser(user);
                break;
            case "replier":
                String groupName = (String) map.get("groupName");
                Answer replier = Answer.Build()
                        .id(id)
                        .group_id(groupName)
                        .build();
                answerMapper.insertAnswer(replier);
                break;
            default:
                return false;
        }
        return true;
    }

    public Boolean secRetrieval(Map<String, Object> map) {
        String account = (String) map.get("account");
        String securityAnswer = (String) map.get("securityAnswer");
        String newPassword = (String) map.get("newPassword");

        TotalRole totalRole = TotalRole.QueryBuild()
                .account(account).fetchAll().build();
        TotalRole res = totalRoleMapper.queryTotalRoleLimit1(totalRole);
        if (!res.getSecurity_answer().equals(securityAnswer)) {
            return false;
        }
        return true;
    }

    public Boolean smsRetrieval(Map<String, Object> map) {
        String phoneNumber = (String) map.get("phoneNumber");
        String veriCode = (String) map.get("veriCode");
        String newPassword = (String) map.get("newPassword");
        return true;
    }

    public List queryAll(String phoneNumber, String account, String id) {
        TotalRole totalRole = TotalRole
                .QueryBuild()
                .phone_number(phoneNumber)
                .account(account)
                .id(id)
                .fetchAll()
                .build();
        List<TotalRole> listTotal = totalRoleMapper.queryTotalRole(totalRole);
        List res = new ArrayList();
        for (TotalRole t : listTotal) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("id", t.getId());
            map.put("role", t.getRole());
            map.put("account", t.getAccount());
            map.put("phoneNumber", t.getPhone_number());
            map.put("status", t.getStatus());
            map.put("securityQuestion", t.getSecurity_question());
            map.put("securityAnswer", t.getSecurity_answer());
            switch (Role.values()[t.getRole()].name().toLowerCase()) {
                case "admin":
                    break;
                case "tenant":
                    Tenant queryTenant = Tenant.QueryBuild().id(t.getId()).fetchAll().build();
                    Tenant tenant = tenantMapper.queryTenantLimit1(queryTenant);
                    map.put("bill", tenant.getBill());
                    // todo: add authority
                    map.put("authority", true);
                    break;
                case "user":
                    User queryUser = User.QueryBuild().id(t.getId()).fetchAll().build();
                    User user = userMapper.queryUserLimit1(queryUser);
                    map.put("tenantId", user.getTenant_id());
                    map.put("surveyLimit", user.getTenant_id());
                    break;
                case "replier":
                    Answer queryReplier = Answer.QueryBuild().id(t.getId()).fetchAll().build();
                    Answer replier = answerMapper.queryAnswerLimit1(queryReplier);
                    // todo: add groupIdList
                    map.put("groupIdList", new ArrayList<>());
                    break;
                default:
                    break;
            }
            res.add(map);
        }
        return res;
    }
}
