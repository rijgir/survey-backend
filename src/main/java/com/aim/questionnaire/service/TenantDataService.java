package com.aim.questionnaire.service;

import com.aim.questionnaire.common.Role;
import com.aim.questionnaire.common.utils.UUIDUtil;
import com.aim.questionnaire.dao.TenantMapper;
import com.aim.questionnaire.dao.TotalRoleMapper;
import com.aim.questionnaire.dao.entity.Tenant;
import com.aim.questionnaire.dao.entity.TotalRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class TenantDataService {
    @Autowired
    private TenantMapper tenantMapper;

    @Autowired
    private TotalRoleMapper totalRoleMapper;


    public Boolean addTenant(Map<String, Object> map) {
        String account = (String) map.get("account");
        String password = (String) map.get("password");
        String phoneNumber = (String) map.get("phoneNumber");

        String id = UUIDUtil.getOneUUID();
        Tenant tenant = Tenant.Build()
                .id(id)
                .bill(0.0)
                .build();
        tenantMapper.insertTenant(tenant);

        TotalRole totalRole = TotalRole.Build()
                .id(id)
                .account(account)
                .password(password)
                .role(Role.TENANT.ordinal())
                .phone_number(phoneNumber)
                .status(1)
                .security_question("")
                .security_answer("")
                .deleted(0)
                .build();
        totalRoleMapper.insertTotalRole(totalRole);
        return true;
    }

    public List<Object> queryTenantList(String phoneNumber, String account) {
        TotalRole totalRole = TotalRole.QueryBuild().phone_number(phoneNumber).account(account).fetchAll().build();
        List<TotalRole> a = totalRoleMapper.queryTotalRole(totalRole);
        List<Object> arrayList = new ArrayList();
        for (TotalRole arr : a) {
            Map<String, Object> map = new HashMap<>();
            String m = arr.getId();
            Tenant tenant = Tenant.QueryBuild().id(m).fetchAll().build();
            Tenant n = tenantMapper.queryTenantLimit1(tenant);
            if (n == null) {
                continue;
            }
            map.put("id", m);
            map.put("account", arr.getAccount());
            map.put("phoneNumber", arr.getPhone_number());
            map.put("bill", n.getBill());
            map.put("status", arr.getStatus());
            map.put("securityQuestion", arr.getSecurity_question());
            map.put("securityAnswer", arr.getSecurity_answer());
            map.put("deleted", arr.getDeleted());
            arrayList.add(map);
        }
        return arrayList;
    }

    public int deleteTenant(String id) {
        TotalRole totalRole = TotalRole.Build().id(id).deleted(1).build();
        totalRoleMapper.updateTotalRole(totalRole);
        return 0;
    }

    public int modifyTenant(Map<String, Object> map) {
        String id = (String) map.get("id");
        String phoneNumber = (String) map.get("phoneNumber");
        String password = (String) map.get("password");
        String securityQuestion = (String) map.get("securityQuestion");
        String securityAnswer = (String) map.get("securityAnswer");
        Integer deleted = (Integer) map.get("deleted");
        TotalRole totalRole = TotalRole.Build().id(id)
                .phone_number(phoneNumber).password(password)
                .security_question(securityQuestion)
                .security_answer(securityAnswer)
                .deleted(deleted).build();

        totalRoleMapper.updateTotalRole(totalRole);
        return 0;
    }
}
