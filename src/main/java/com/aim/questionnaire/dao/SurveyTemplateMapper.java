package com.aim.questionnaire.dao;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.aim.questionnaire.dao.entity.SurveyTemplate;
import com.aim.questionnaire.dao.base.SurveyTemplateBaseMapper;
/**
*  @author author
*/
public interface SurveyTemplateMapper extends SurveyTemplateBaseMapper{

    int deleteSurveyTemplate(@Param("id") String id);

}
