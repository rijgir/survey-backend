package com.aim.questionnaire.dao.base;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.aim.questionnaire.dao.entity.UserData;
/**
*  @author author
*/
public interface UserDataBaseMapper {

    int insertUserData(UserData object);

    int updateUserData(UserData object);

    int update(UserData.UpdateBuilder object);

    List<UserData> queryUserData(UserData object);

    UserData queryUserDataLimit1(UserData object);

}
