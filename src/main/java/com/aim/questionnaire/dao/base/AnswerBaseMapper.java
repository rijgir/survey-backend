package com.aim.questionnaire.dao.base;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.aim.questionnaire.dao.entity.Answer;
/**
*  @author author
*/
public interface AnswerBaseMapper {

    int insertAnswer(Answer object);

    int updateAnswer(Answer object);

    int update(Answer.UpdateBuilder object);

    List<Answer> queryAnswer(Answer object);

    Answer queryAnswerLimit1(Answer object);

}