package com.aim.questionnaire.dao.base;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.aim.questionnaire.dao.entity.SurveyLinked;
/**
*  @author author
*/
public interface SurveyLinkedBaseMapper {

    int insertSurveyLinked(SurveyLinked object);

    int updateSurveyLinked(SurveyLinked object);

    int update(SurveyLinked.UpdateBuilder object);

    List<SurveyLinked> querySurveyLinked(SurveyLinked object);

    SurveyLinked querySurveyLinkedLimit1(SurveyLinked object);

}
