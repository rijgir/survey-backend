package com.aim.questionnaire.controller;

import com.aim.questionnaire.common.Constans;
import com.aim.questionnaire.service.ReplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
public class ReplierController {

    @Autowired
    private ReplierService replierService;

    @RequestMapping(value = "/addReplier", method = RequestMethod.POST, headers = "Accept=application/json")
    public ReplierHttpResponseEntity addReplier(@RequestBody Map<String, Object> map) {
        ReplierHttpResponseEntity replierHttpResponseEntity = new ReplierHttpResponseEntity();
        try {
            Boolean res = replierService.addReplier(map);
            if (res) {
                replierHttpResponseEntity.setCode(Integer.toString(801));
            } else {
                replierHttpResponseEntity.setCode(Constans.EXIST_CODE);
            }
        } catch (Exception e) {
            replierHttpResponseEntity.setCode(Constans.EXIST_CODE);
        }
        return replierHttpResponseEntity;
    }

    @RequestMapping(value = "/addMultiReplierByEx", method = RequestMethod.POST, headers = "Accept=application/json")
    public ReplierHttpResponseEntity addMultiReplierByEx(@RequestBody Map<String, Object> map) {
        ReplierHttpResponseEntity replierHttpResponseEntity = new ReplierHttpResponseEntity();
        try {
            Boolean res = replierService.addMultiReplierByEx(map);
            if (res) {
                replierHttpResponseEntity.setCode(Integer.toString(801));
            } else {
                replierHttpResponseEntity.setCode(Constans.EXIST_CODE);
            }
        } catch (Exception e) {
            replierHttpResponseEntity.setCode(Constans.EXIST_CODE);
        }
        return replierHttpResponseEntity;
    }

    @RequestMapping(value = "/addMultiReplierById", method = RequestMethod.POST, headers = "Accept=application/json")
    public ReplierHttpResponseEntity addMultiReplierById(@RequestBody Map<String, Object> map) {
        ReplierHttpResponseEntity replierHttpResponseEntity = new ReplierHttpResponseEntity();
        try {
            Boolean res = replierService.addMultiReplierById(map);
            if (res) {
                replierHttpResponseEntity.setCode(Integer.toString(801));
            } else {
                replierHttpResponseEntity.setCode(Constans.EXIST_CODE);
            }
        } catch (Exception e) {
            replierHttpResponseEntity.setCode(Constans.EXIST_CODE);
        }
        return replierHttpResponseEntity;
    }

    @RequestMapping(value = "/queryReplier", method = RequestMethod.GET, headers = "Accept=application/json")
    public ReplierHttpResponseEntity queryReplier(
            @RequestParam(required = false, name = "groupId") String groupId,
            @RequestParam(required = false, name = "phoneNumber") String phoneNumber) {
        ReplierHttpResponseEntity replierHttpResponseEntity = new ReplierHttpResponseEntity();
        try {
            List list = replierService.queryReplier(groupId, phoneNumber);
            replierHttpResponseEntity.setReplierList(list);
            replierHttpResponseEntity.setCode(Integer.toString(801));
        } catch (Exception e) {
            replierHttpResponseEntity.setCode(Constans.EXIST_CODE);
        }
        return replierHttpResponseEntity;
    }

    @RequestMapping(value = "/queryReplierList", method = RequestMethod.GET, headers = "Accept=application/json")
    public ReplierHttpResponseEntity queryReplierList(
            @RequestParam(required = false, name = "groupIdList") ArrayList<String> groupIdList,
            @RequestParam(required = false, name = "phoneNumber") String phoneNumber) {
        ReplierHttpResponseEntity replierHttpResponseEntity = new ReplierHttpResponseEntity();
        try {
            List list = replierService.queryReplierList(groupIdList, phoneNumber);
            replierHttpResponseEntity.setReplierList(list);
            replierHttpResponseEntity.setCode(Integer.toString(801));
        } catch (Exception e) {
            replierHttpResponseEntity.setCode(Constans.EXIST_CODE);
        }
        return replierHttpResponseEntity;
    }

    @RequestMapping(value = "/modifyReplier", method = RequestMethod.PUT, headers = "Accept=application/json")
    public ReplierHttpResponseEntity modifyReplier(@RequestBody Map<String, Object> map) {
        ReplierHttpResponseEntity replierHttpResponseEntity = new ReplierHttpResponseEntity();
        try {
            replierService.modifyReplier(map);
            replierHttpResponseEntity.setCode(Integer.toString(801));
        } catch (Exception e) {
            replierHttpResponseEntity.setCode(Constans.EXIST_CODE);
        }
        return replierHttpResponseEntity;
    }


    @RequestMapping(value = "/deleteReplier", method = RequestMethod.DELETE, headers = "Accept=application/json")
    public ReplierHttpResponseEntity deleteReplier(@RequestParam("id") String id) {
        ReplierHttpResponseEntity replierHttpResponseEntity = new ReplierHttpResponseEntity();
        try {
            Boolean res = replierService.deleteReplier(id);
            if (res) {
                replierHttpResponseEntity.setCode(Integer.toString(801));
            } else {
                replierHttpResponseEntity.setCode(Constans.EXIST_CODE);
            }
        } catch (Exception e) {
            replierHttpResponseEntity.setCode(Constans.EXIST_CODE);
        }
        return replierHttpResponseEntity;
    }

}

class ReplierHttpResponseEntity implements Serializable {

    private String code;
    private Object replierList;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Object getReplierList() {
        return replierList;
    }

    public void setReplierList(Object replierList) {
        this.replierList = replierList;
    }
}
