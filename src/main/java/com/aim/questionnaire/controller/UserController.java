package com.aim.questionnaire.controller;

import com.aim.questionnaire.common.Constans;
import com.aim.questionnaire.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/addUser", method = RequestMethod.POST, headers = "Accept=application/json")
    public UserHttpResponseEntity addUser(@RequestBody Map<String, Object> map) {
        UserHttpResponseEntity userHttpResponseEntity = new UserHttpResponseEntity();
        try {
            Boolean res = userService.addUser(map);
            if (res) {
                userHttpResponseEntity.setCode(Integer.toString(801));
            } else {
                userHttpResponseEntity.setCode(Constans.EXIST_CODE);
            }
        } catch (Exception e) {
            userHttpResponseEntity.setCode(Constans.EXIST_CODE);
        }
        return userHttpResponseEntity;
    }

    @RequestMapping(value = "/addMultiUserByEx", method = RequestMethod.POST, headers = "Accept=application/json")
    public UserHttpResponseEntity addMultiUserByEx(@RequestBody Map<String, Object> map) {
        UserHttpResponseEntity userHttpResponseEntity = new UserHttpResponseEntity();
        try {
            Boolean res = userService.addMultiUserByEx(map);
            if (res) {
                userHttpResponseEntity.setCode(Integer.toString(801));
            } else {
                userHttpResponseEntity.setCode(Constans.EXIST_CODE);
            }
        } catch (Exception e) {
            userHttpResponseEntity.setCode(Constans.EXIST_CODE);
        }
        return userHttpResponseEntity;
    }

    @RequestMapping(value = "/queryUserList", method = RequestMethod.GET, headers = "Accept=application/json")
    public UserHttpResponseEntity queryUserList(
            @RequestParam(required = false, name = "tenantId") String tenantId,
            @RequestParam(required = false, name = "phoneNumber") String phoneNumber) {
        UserHttpResponseEntity userHttpResponseEntity = new UserHttpResponseEntity();
        try {
            List list = userService.queryUserList(tenantId, phoneNumber);
            userHttpResponseEntity.setUserList(list);
            userHttpResponseEntity.setCode(Integer.toString(801));
        } catch (Exception e) {
            userHttpResponseEntity.setCode(Constans.EXIST_CODE);
        }
        return userHttpResponseEntity;
    }

    @RequestMapping(value = "/modifyUser", method = RequestMethod.PUT, headers = "Accept=application/json")
    public UserHttpResponseEntity modifyUser(@RequestBody Map<String, Object> map) {
        UserHttpResponseEntity userHttpResponseEntity = new UserHttpResponseEntity();
        try {
            Boolean res = userService.modifyUser(map);
            if (res) {
                userHttpResponseEntity.setCode(Integer.toString(801));
            } else {
                userHttpResponseEntity.setCode(Constans.EXIST_CODE);
            }
        } catch (Exception e) {
            userHttpResponseEntity.setCode(Constans.EXIST_CODE);
        }
        return userHttpResponseEntity;
    }


    @RequestMapping(value = "/deleteUser", method = RequestMethod.DELETE, headers = "Accept=application/json")
    public UserHttpResponseEntity deleteUser(@RequestParam("id") String id) {
        UserHttpResponseEntity userHttpResponseEntity = new UserHttpResponseEntity();
        try {
            int res = userService.deleteUser(id);
            if (res != 0) {
                userHttpResponseEntity.setCode(Constans.EXIST_CODE);
            } else {
                userHttpResponseEntity.setCode(Integer.toString(801));
            }
        } catch (Exception e) {
            userHttpResponseEntity.setCode(Constans.EXIST_CODE);
        }
        return userHttpResponseEntity;
    }

}

class UserHttpResponseEntity implements Serializable {

    private String code;
    private Object userList;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Object getUserList() {
        return userList;
    }

    public void setUserList(Object userList) {
        this.userList = userList;
    }
}
