package com.aim.questionnaire.controller;

import com.aim.questionnaire.common.Constans;
import com.aim.questionnaire.dao.entity.ProjectData;
import com.aim.questionnaire.service.GroupDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@RestController
public class GroupDataController {
    @Autowired
    private GroupDataService groupDataService;

    @RequestMapping(value = "/addGroup", method = RequestMethod.POST, headers = "Accept=application/json")
    public GroupHttpResponseEntity addGroup(@RequestBody Map<String, Object> map) {
        GroupHttpResponseEntity groupHttpResponseEntity = new GroupHttpResponseEntity();
        try {
            groupDataService.addGroup(map);
            groupHttpResponseEntity.setCode(Integer.toString(801));
        } catch (Exception e) {
            groupHttpResponseEntity.setCode(Constans.EXIST_CODE);
        }
        return groupHttpResponseEntity;
    }

    @RequestMapping(value = "/deleteGroup", method = RequestMethod.DELETE, headers = "Accept=application/json")
    public GroupHttpResponseEntity deleteGroup(@RequestParam("id") String id) {
        GroupHttpResponseEntity groupHttpResponseEntity = new GroupHttpResponseEntity();
        try {
            int res = groupDataService.deleteGroup(id);
            if (res != 0) {
                groupHttpResponseEntity.setCode(Constans.EXIST_CODE);
            } else {
                groupHttpResponseEntity.setCode(Integer.toString(801));
            }
        } catch (Exception e) {
            groupHttpResponseEntity.setCode(Constans.EXIST_CODE);
        }
        return groupHttpResponseEntity;
    }

    @RequestMapping(value = "/modifyGroup", method = RequestMethod.PUT, headers = "Accept=application/json")
    public GroupHttpResponseEntity modifyGroup(@RequestBody Map<String, Object> map) {
        GroupHttpResponseEntity groupHttpResponseEntity = new GroupHttpResponseEntity();
        try {
            groupDataService.modifyGroup(map);
            groupHttpResponseEntity.setCode(Integer.toString(801));
        } catch (Exception e) {
            groupHttpResponseEntity.setCode(Constans.EXIST_CODE);
        }
        return groupHttpResponseEntity;
    }

    @RequestMapping(value = "/queryGroupList", method = RequestMethod.GET, headers = "Accept=application/json")
    public GroupHttpResponseEntity queryGroupList(@RequestParam(required = false, name = "userId") String userId,
                                                  @RequestParam(required = false, name = "tenantId") String tenantId,
                                                  @RequestParam(required = false, name = "groupName") String groupName) {
        GroupHttpResponseEntity groupHttpResponseEntity = new GroupHttpResponseEntity();
        try {
            List res = groupDataService.queryGroupList(userId, tenantId, groupName);
            groupHttpResponseEntity.setGroupList(res);
            groupHttpResponseEntity.setCode(Integer.toString(801));
        } catch (Exception e) {
            groupHttpResponseEntity.setCode(Constans.EXIST_CODE);
        }
        return groupHttpResponseEntity;
    }

}

class GroupHttpResponseEntity implements Serializable {
    private String code;
    private String message;

    private Object groupList;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setGroupList(Object groupList) {
        this.groupList = groupList;
    }

    public Object getGroupList() {
        return this.groupList;
    }
}